import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { AuthorizationService } from './core/authorization.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements
  OnInit,
  OnChanges {
  title = 'Courses-portal';

  public isLogedIn: boolean;

  constructor(private authorizationService: AuthorizationService) { }

  ngOnInit() {
    this.authorizationService.isAuthenticated()
      .subscribe(
        (res) => {
          this.isLogedIn = res;
        },
        (err) => console.log(err)
      );
  }

  ngOnChanges(changes: SimpleChanges) {

  }
}
