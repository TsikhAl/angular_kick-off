import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoursePageComponent } from './course-page/course-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AddEditCourseComponent } from './course-page/add-edit-course/add-edit-course.component';
import { AuthorizationGuard } from './core/authorization.guard';

const routes: Routes = [
  {
    path: 'courses',
    canActivate: [AuthorizationGuard],
    component: CoursePageComponent
  },
  {
    path: 'login',
    component: LoginPageComponent
  },
  {
    path: 'new-course',
    canActivate: [AuthorizationGuard],
    component: AddEditCourseComponent
  },
  {
    path: 'edit-course/:id',
    canActivate: [AuthorizationGuard],
    component: AddEditCourseComponent
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
