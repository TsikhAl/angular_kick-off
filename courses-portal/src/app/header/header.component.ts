import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from '../core/authorization.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {

  public isLogedIn: boolean;

  constructor(private authorizationService: AuthorizationService) { }

  ngOnInit() {
    this.authorizationService.isAuthenticated()
      .subscribe(
        (res) => {
          this.isLogedIn = res;
        },
        (err) => console.log(err)
      );
  }
}
