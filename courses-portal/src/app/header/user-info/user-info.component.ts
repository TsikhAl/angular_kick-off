import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthorizationService } from '../../core/authorization.service';
import { User } from '../../core/user.interface';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.less']
})
export class UserInfoComponent implements OnInit {

  constructor(
    private authorizationService: AuthorizationService,
    private router: Router
    ) { }

  public user: {};
  private userToken: string;

  public logOut(): void {
    this.authorizationService.logOut();

    this.router.navigate(['/login']);
  }

  private getUserToken(): void {
    let authToken = window.localStorage.getItem('userToken');
    authToken = JSON.parse(authToken);
    this.userToken = (authToken && authToken.token) ? authToken.token : null;
  }

  private getUserInfo(): void {
    this.authorizationService.getUserInfo(this.userToken)
    .subscribe(
      (data) => {
        this.user = data;
      },
      (error) => console.log(error)
    );
  }

  ngOnInit() {
    this.getUserToken();
    this.getUserInfo();
  }

}
