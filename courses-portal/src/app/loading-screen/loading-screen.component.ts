import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, from } from 'rxjs';
import { LoadingScreenService } from '../core/loading-screen.service';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-loading-screen',
  templateUrl: './loading-screen.component.html',
  styleUrls: ['./loading-screen.component.less']
})
export class LoadingScreenComponent implements
  OnInit,
  OnDestroy {

  public loading: boolean = false;
  private loadingSubscription: Subscription;
   constructor(private loadingScreenService: LoadingScreenService) {
  }

   ngOnInit() {
    this.loadingSubscription = this.loadingScreenService.loadingStatus$
      .pipe(
        debounceTime(3000)
      ).subscribe((value: boolean) => {
        this.loading = value;
      });
  }

   ngOnDestroy() {
    this.loadingSubscription.unsubscribe();
  }

}
