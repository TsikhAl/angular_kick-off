import { Injectable } from '@angular/core';
import { CourseItem } from './course-item.interface';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
// import { FilterCoursesByTitlePipe } from '../course-page/courses-list/filter-courses-by-title.pipe';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CoursesListItems {
  private availableCourses: CourseItem[];
  private url: string = environment.url;

  constructor(
    // private filterByTitlePipe: FilterCoursesByTitlePipe,
    private http: HttpClient
  ) { }

  public getAllCourses(from: number, count: number, textFragment?: string): Observable<CourseItem[]> {
    const params = new HttpParams()
      .set('start', (from + ''))
      .set('count', (count + ''))
      .set('textFragment', textFragment);

    return this.http.get<CourseItem[]>(this.url, {params});
  }

  public createCourse(newCourse: CourseItem): Observable<CourseItem> {
    return this.http.post<CourseItem>(this.url, newCourse);
  }

  public getCourseById(id: number): Observable<CourseItem> | undefined {
    const params = new HttpParams()
      .set('id', (id + ''));

    return this.http.get<CourseItem>(this.url, {params});
  }

  public updateCourse(id: number): Observable<CourseItem> {
    const params = new HttpParams()
      .set('id', (id + ''));

    return this.http.patch<CourseItem>(this.url, {params});
  }

  public removeCourse(id: number): Observable<CourseItem> {
    const params = new HttpParams()
      .set('id', (id + ''));

    return this.http.delete<CourseItem>(this.url, {params});
  }
}
