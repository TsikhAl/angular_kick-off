import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../core/user.interface';
import { Observable, from } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  constructor(private http: HttpClient) { }

  public logIn(userName: string, userPass: string): Observable<string> {
      const body = {login: userName, password: userPass};

      return this.http.post('http://localhost:3004/auth/login', body, {responseType: 'text'})
        .pipe(
          tap((data: string) => {
            const resp = JSON.parse(data);
            window.localStorage.setItem('userToken', JSON.stringify( {token: resp.token} ));
          })
        );
  }

  public logOut(): void {
    this.isAuthenticated()
      .subscribe(
        (res) => {
          if (res) {
            localStorage.removeItem('userToken');
          }
        },
        (err) => console.log(err)
      );
  }

  public isAuthenticated(): Observable<boolean> {
    const res = [];

    if (localStorage.getItem('userToken') !== null) {
      res.push(true);
    } else {
      res.push(false);
    }

    return from(res);
  }

  public getUserInfo(userToken: string | null): Observable<any> { // User[] ?
    const body = {token: userToken};
    return this.http.post('http://localhost:3004/auth/userinfo', body);
  }
}
