import { TestBed } from '@angular/core/testing';

import { CoursesListItems } from './courses-list-items.service';

describe('CoursesListItemsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CoursesListItems = TestBed.get(CoursesListItems);
    expect(service).toBeTruthy();
  });
});
