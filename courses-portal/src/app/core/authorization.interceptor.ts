import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  private getUserToken(): string | null {
    let authToken = window.localStorage.getItem('userToken');
    authToken = JSON.parse(authToken);
    return (authToken && authToken.token) ? authToken.token : null;
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const userToken = this.getUserToken();
    const value = (userToken) ? userToken : '';

    const authReq = req.clone({
        headers: req.headers.set('Authorization', value)
    });

    return next.handle(authReq);
  }
}
