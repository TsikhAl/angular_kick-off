interface IUser {
    id: number;
    fakeToken: string;
    name: {
      first: string,
      last: string
    };
    login: string;
    password: string;
}

export class User implements IUser {
  id: number;
  fakeToken: string;
  name: {
    first: string,
    last: string
  };
  login: string;
  password: string;

  constructor(arg: IUser) {
      this.id = arg.id;
      this.fakeToken = arg.fakeToken;
      this.name = arg.name;
      this.login = arg.login;
      this.password = arg.password;
  }
}
