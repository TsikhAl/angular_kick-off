interface ICourseItem {
    id: number;
    name: string;
    length: number;
    date: string;
    description: string;
    isTopRated: boolean;
    authors: [
        {
            id: number,
            name: string,
            lastName: string
        }
    ];
}

export class CourseItem implements ICourseItem {
    id: number;
    name: string;
    length: number;
    date: string;
    description: string;
    isTopRated: boolean;
    authors: [
        {
            id: number,
            name: string,
            lastName: string
        }
    ];

    constructor(arg: ICourseItem) {
        this.id = arg.id;
        this.name = arg.name;
        this.length = arg.length;
        this.date = arg.date;
        this.description = arg.description;
        this.isTopRated = arg.isTopRated;
        this.authors = arg.authors;
    }
}
