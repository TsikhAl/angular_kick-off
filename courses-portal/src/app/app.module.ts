import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { CoursePageComponent } from './course-page/course-page.component';
import { FooterComponent } from './footer/footer.component';
import { LogoComponent } from './header/logo/logo.component';
import { UserInfoComponent } from './header/user-info/user-info.component';
import { SearchBoxComponent } from './course-page/search-box/search-box.component';
import { CoursesListComponent } from './course-page/courses-list/courses-list.component';
import { CourseItemComponent } from './course-page/courses-list/course-item/course-item.component';
import { BreadcrumbsComponent } from './course-page/breadcrumbs/breadcrumbs.component';
import { LoadMoreComponent } from './course-page/load-more/load-more.component';
import { CoursesUnavailableComponent } from './course-page/courses-list/courses-unavailable/courses-unavailable.component';
import { ChangeBorderBasedOnDateDirective } from './course-page/courses-list/change-border-based-on-date.directive';
import { TimeDurationPipe } from './course-page/courses-list/time-duration.pipe';
import { OrderByDatePipe } from './course-page/courses-list/order-by-date.pipe';
import { FilterCoursesByTitlePipe } from './course-page/courses-list/filter-courses-by-title.pipe';
import { LoginPageComponent } from './login-page/login-page.component';
import { AddEditCourseComponent } from './course-page/add-edit-course/add-edit-course.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CourseDurationComponent } from './course-page/add-edit-course/course-duration/course-duration.component';
import { CourseAuthorComponent } from './course-page/add-edit-course/course-author/course-author.component';
import { CourseDateCreationComponent } from './course-page/add-edit-course/course-date-creation/course-date-creation.component';
import { AuthInterceptor } from './core/authorization.interceptor';
import { LoadingScreenComponent } from './loading-screen/loading-screen.component';
import { LoadingScreenInterceptor } from './core/loading.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CoursePageComponent,
    FooterComponent,
    LogoComponent,
    UserInfoComponent,
    SearchBoxComponent,
    CoursesListComponent,
    CourseItemComponent,
    BreadcrumbsComponent,
    LoadMoreComponent,
    CoursesUnavailableComponent,
    ChangeBorderBasedOnDateDirective,
    TimeDurationPipe,
    OrderByDatePipe,
    FilterCoursesByTitlePipe,
    LoginPageComponent,
    AddEditCourseComponent,
    PageNotFoundComponent,
    CourseDurationComponent,
    CourseAuthorComponent,
    CourseDateCreationComponent,
    LoadingScreenComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    FilterCoursesByTitlePipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingScreenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
