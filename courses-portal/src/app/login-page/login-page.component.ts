import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AuthorizationService } from '../core/authorization.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.less']
})
export class LoginPageComponent {
  @Input() inputEmail: string = null;
  @Input() inputPassword: string = null;

  public isInfoCorrect: boolean = true;

  constructor(
    private authorizationService: AuthorizationService,
    private router: Router
    ) { }

  public loginUser(): void {
    this.authorizationService
      .logIn(this.inputEmail, this.inputPassword)
      .subscribe(
        () => this.router.navigate(['/courses']),
        (error) => {
          this.isInfoCorrect = false;
          this.inputEmail = null;
          this.inputPassword = null;
          console.log(error);
        }
      );
  }
}
