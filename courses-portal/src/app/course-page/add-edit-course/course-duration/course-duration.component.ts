import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-course-duration',
  templateUrl: './course-duration.component.html',
  styleUrls: ['./course-duration.component.less']
})
export class CourseDurationComponent implements OnInit {
  @Input() courseDuration: number = null;

  @Output() updatedDuration: EventEmitter<object> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  public leaveInput(value: string): void {
    if (value && value.length) {
      this.courseDuration = +value;
      this.updatedDuration.emit({courseDuration: this.courseDuration, inputType: 'courseDuration'});
    }
  }
}
