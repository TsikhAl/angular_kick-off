import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CourseItem } from '../../core/course-item.interface';
import { CoursesListItems } from '../../core/courses-list-items.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-edit-course',
  templateUrl: './add-edit-course.component.html',
  styleUrls: ['./add-edit-course.component.less']
})
export class AddEditCourseComponent implements
  OnInit {
  @Input() inputTitle: string;
  @Input() inputDescription: string;
  @Input() inputDuration: number;
  @Input() inputDate: string;
  @Input() inputAuthors: string;

  public idFromUrl: number | undefined;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private coursesItemsService: CoursesListItems
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((urlParams) => {
      this.idFromUrl = +urlParams.id;
    });

    if (this.idFromUrl) {
      this.editCourse(this.idFromUrl);
    }
  }

  public save(): void {
    const course: CourseItem = {
      id: null,
      name: this.inputTitle,
      length: this.inputDuration,
      date: this.inputDate,
      description: this.inputDescription,
      isTopRated: null,
      authors: null
    };

    if (this.idFromUrl) {
      course.id = this.idFromUrl;
      this.coursesItemsService.updateCourse(this.idFromUrl)
        .subscribe(
          () => console.log('Course has been updated!'),
          (err) => console.log(err)
        );
    }
    else {
      course.id = Math.round(Math.random() * 1000);

      this.coursesItemsService.createCourse(course)
        .subscribe(
          () => console.log('Course has been added!'),
          (error) => console.log(error)
        );
    }

    this.router.navigate(['/courses']);
  }

  public cancel(): void {
    this.router.navigate(['/courses']);
  }

  public editCourse(id: number): void {
    this.coursesItemsService.getCourseById(id)
      .subscribe(
        (courses) => {
          if (courses.length) {
            this.inputTitle = courses[0].name;
            this.inputDescription = courses[0].description;
            this.inputDuration = courses[0].length;
            this.inputDate = courses[0].date;
          }
        },
        (err) => console.log(err)
      );
  }

  public setCourseValue(inputValue: any): void {
    if (inputValue.inputType === 'courseDuration') {
      this.inputDuration = inputValue.courseDuration;
    }
    else if (inputValue.inputType === 'courseDate') {
      this.inputDate = inputValue.courseDate;
    }

  }
}
