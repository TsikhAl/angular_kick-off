import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-course-author',
  templateUrl: './course-author.component.html',
  styleUrls: ['./course-author.component.less']
})
export class CourseAuthorComponent implements OnInit {
  @Input() public inputAuthors: string = null;

  constructor() { }

  ngOnInit() {
  }

}
