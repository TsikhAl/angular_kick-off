import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-course-date-creation',
  templateUrl: './course-date-creation.component.html',
  styleUrls: ['./course-date-creation.component.less']
})
export class CourseDateCreationComponent implements OnInit {
  @Input() courseDate: string = null;

  @Output() updatedDate: EventEmitter<object> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  public leaveInput(value: string): void {
    if (value && value.length) {
      this.courseDate = value;
      this.updatedDate.emit({courseDate: this.courseDate, inputType: 'courseDate'});
    }
  }
}
