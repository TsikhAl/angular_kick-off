import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseDateCreationComponent } from './course-date-creation.component';

describe('CourseDateCreationComponent', () => {
  let component: CourseDateCreationComponent;
  let fixture: ComponentFixture<CourseDateCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseDateCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseDateCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
