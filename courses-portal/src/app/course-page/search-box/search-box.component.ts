import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { map, distinctUntilChanged, debounceTime, filter } from 'rxjs/operators';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.less']
})
export class SearchBoxComponent implements
  OnInit,
  OnDestroy {
  private subject$ = new Subject();
  public inputValue: string;

  @Output() searchCoursesByTitle: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit() {

    this.subject$
      .pipe(
        map(() => {
          if (this.inputValue.length >= 3) {
            return this.inputValue;
          }
        }),
        distinctUntilChanged(),
        debounceTime(1000)
      )
    .subscribe(
      (value: string) => this.searchCoursesByTitle.emit(value),
      (err) => console.log(err)
    );

  }

  ngOnDestroy(): void {
    this.subject$.unsubscribe();
  }

  public search(): void {
    this.subject$.next(this.inputValue);
  }
}
