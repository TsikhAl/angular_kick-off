import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

import { SearchBoxComponent } from './search-box.component';

describe('SearchBoxComponent', () => {
  let component: SearchBoxComponent;
  let fixture: ComponentFixture<SearchBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule ],
      declarations: [ SearchBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.search = function () {};
  });

  it('1. should create', () => {
    expect(component).toBeTruthy();
  });

  it('2. should check that search method was called and input value is not empty', () => {
    let inputValue: string;
    
    let componentDebugElemBtnSearch  = fixture.debugElement.query(By.css('.search-box > button'));
    let componentNativeElemBtnSearch = componentDebugElemBtnSearch.nativeElement;

    spyOn(component, 'search');
    inputValue = 'Value';
    componentNativeElemBtnSearch.click();
    expect(component.search).toHaveBeenCalled();
    expect(inputValue).toBeDefined();
    expect(inputValue.length).toBeGreaterThan(0);
  });
});
