import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeDuration'
})
export class TimeDurationPipe implements PipeTransform {

  transform(duration: number): string {
    const hours: number = Math.floor(duration / 60);

    if (duration < 60) {
      return (duration - hours * 60) + 'min';
    } else {
      return hours + 'h ' + (duration - hours * 60) + 'min';
    }
  }

}
