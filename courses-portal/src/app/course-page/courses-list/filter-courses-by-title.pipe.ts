import { Pipe, PipeTransform } from '@angular/core';
import { CourseItem } from '../../core/course-item.interface';

@Pipe({
  name: 'filterCoursesByTitle'
})
export class FilterCoursesByTitlePipe implements PipeTransform {

  transform(array: any, value: string): any {
    if (Array.isArray(array) && !array.length) {
      return array;
    }

    return array.filter((item: CourseItem) => {
      const name = item.name.toLowerCase();
      const result = name.includes(value.toLowerCase());

      return result;
    });
  }
}
