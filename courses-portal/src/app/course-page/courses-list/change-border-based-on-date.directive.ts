import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appChangeBorderBasedOnDate]'
})
export class ChangeBorderBasedOnDateDirective implements OnInit {

  @Input('appChangeBorderBasedOnDate') public creationDate: string;
  private currentDate: Date;

  constructor(private element: ElementRef) { }

  ngOnInit() {
    this.currentDate = new Date();
    this.changeBorder(this.compareDates(this.creationDate));
  }

  private compareDates(creation: string): string {
    const creationDate = new Date(creation);
    const currentDateWithSubtraction = new Date();
    currentDateWithSubtraction.setDate(currentDateWithSubtraction.getDate() - 14);

    if (creationDate > this.currentDate) {
      return 'upcoming course';
    }
    else if (creationDate < this.currentDate && creationDate >= currentDateWithSubtraction) {
      return 'fresh course';
    }
    else {
      return '';
    }
  }

  private changeBorder(courseStatus: string): void {

    if (courseStatus && courseStatus === 'fresh course') {
      this.element.nativeElement.style.borderColor = 'green';
    }
    else if (courseStatus && courseStatus === 'upcoming course') {
      this.element.nativeElement.style.borderColor = 'blue';
    }
    else {
      this.element.nativeElement.style.border = 'none';
    }
  }

}
