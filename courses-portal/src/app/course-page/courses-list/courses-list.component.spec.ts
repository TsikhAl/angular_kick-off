import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { CoursesListComponent } from './courses-list.component';
import { CourseItemComponent } from './course-item/course-item.component';

describe('CoursesListComponent', () => {
  let component: CoursesListComponent;
  let fixture: ComponentFixture<CoursesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        CoursesListComponent,
        CourseItemComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    let availableCoursesMock = [
      {
        id: 0,
        title: 'First',
        duration: '2h 10min',
        date: '14 Nov, 2017',
        description: `Lorem ipsum dolor sit amet consectetur adipisicing elit.
                      Aliquam nemo dolorum est dicta eos fuga optio similique, nostrum asperiores laudantium?
                      Incidunt pariatur reprehenderit minus odit debitis nisi officiis accusamus sint.
                      Lorem ipsum dolor sit amet consectetur adipisicing elit.
                      Aliquam nemo dolorum est dicta eos fuga optio similique, nostrum asperiores laudantium?
                      Incidunt pariatur reprehenderit minus odit debitis nisi officiis accusamus sint.
                      Lorem ipsum dolor sit amet consectetur adipisicing elit.
                      Aliquam nemo dolorum est dicta eos fuga optio similique, nostrum asperiores laudantium?
                      Incidunt pariatur reprehenderit minus odit debitis nisi officiis accusamus sint.` 
      },
      {
        id: 1,
        title: 'Second',
        duration: '1h 35min',
        date: '9 Dec, 2016',
        description: `Lorem ipsum dolor sit amet consectetur adipisicing elit.
                      Aliquam nemo dolorum est dicta eos fuga optio similique, nostrum asperiores laudantium?
                      Incidunt pariatur reprehenderit minus odit debitis nisi officiis accusamus sint.
                      Lorem ipsum dolor sit amet consectetur adipisicing elit.
                      Aliquam nemo dolorum est dicta eos fuga optio similique, nostrum asperiores laudantium?
                      Incidunt pariatur reprehenderit minus odit debitis nisi officiis accusamus sint.` 
      }
    ];
    component.availableCourses = availableCoursesMock;

    component.addCourse = function() {};
    component.editCourseItem = function(event) {}
  });

  it('1. should create', () => {
    expect(component).toBeTruthy();
  });

  it('2. should check that available courses was passed from the parent component and it is exists', () => {
    expect(component.availableCourses).toBeDefined();
    expect(component.availableCourses.length).toBeGreaterThan(0);
    expect(component.availableCourses[0].id).toBe(0)
  });

  it('3. should check that method addCourse was called', () => {
    const componentDebugElemBtnAddCourse = fixture.debugElement.query(By.css('.add-course > button'));
    const componentNativeElemBtnAddCourse = componentDebugElemBtnAddCourse.nativeElement;

    spyOn(component, 'addCourse');
    componentNativeElemBtnAddCourse.click();

    expect(component.addCourse).toBeDefined();
    expect(component.addCourse).toHaveBeenCalled();
  });

  it('4. should check that method editCourseItem was called', () => {

    // ?
    expect(component.editCourseItem).toBeDefined();
    component.editCourseItem({});
  });
});
