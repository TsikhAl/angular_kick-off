import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesUnavailableComponent } from './courses-unavailable.component';

describe('CoursesUnavailableComponent', () => {
  let component: CoursesUnavailableComponent;
  let fixture: ComponentFixture<CoursesUnavailableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursesUnavailableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesUnavailableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
