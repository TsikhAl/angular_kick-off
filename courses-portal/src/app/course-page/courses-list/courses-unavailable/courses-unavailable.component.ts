import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-courses-unavailable',
  templateUrl: './courses-unavailable.component.html',
  styleUrls: ['./courses-unavailable.component.less']
})
export class CoursesUnavailableComponent implements OnInit {
  public infoMsg: string = 'no data, feel free to add new courses !';

  constructor() { }

  ngOnInit() {
  }

}
