import { Component, Input, Output, OnInit, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { CourseItem } from '../../core/course-item.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-courses-list',
  templateUrl: './courses-list.component.html',
  styleUrls: ['./courses-list.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CoursesListComponent implements OnInit {
  @Input()
  public availableCourses: CourseItem[];

  @Output() edited: EventEmitter<CourseItem> = new EventEmitter();
  @Output() deleted: EventEmitter<CourseItem> = new EventEmitter();

  constructor(
    private router: Router
   ) { }

  ngOnInit() { }

  public addCourse(): void  {
    this.router.navigate(['/new-course']);
  }

  public editCourseItem(courseIndex: number): void {
    this.edited.emit(this.availableCourses[courseIndex]);
  }

  public deleteCourseItem(courseIndex: number): void {
   this.deleted.emit(this.availableCourses[courseIndex]);
  }
}
