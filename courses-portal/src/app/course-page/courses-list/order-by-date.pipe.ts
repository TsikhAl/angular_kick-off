import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderByDate'
})
export class OrderByDatePipe implements PipeTransform {

  transform(array: any, field: string): any[] {
    array.sort((a: any, b: any) => {
      if (new Date(a[field]) < new Date(b[field])) {
        return -1;
      }
      else if (new Date(a[field]) > new Date(b[field])) {
        return 1;
      }
      else {
        return 0;
      }
    });

    return array;
  }
}
