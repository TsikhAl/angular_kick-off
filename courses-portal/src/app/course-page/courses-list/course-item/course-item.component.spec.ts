import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { CourseItemComponent } from './course-item.component';

describe('CourseItemComponent', () => {
  let component: CourseItemComponent;
  let fixture: ComponentFixture<CourseItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseItemComponent);
    component = fixture.componentInstance;

    let courseItemMock = {
      id: 1,
      title: 'Title',
      duration: '30 min',
      date: '11 Aug',
      description: 'XXX'
    };
    component.course = courseItemMock;
    component.deleteCourseItem = function () {};

    fixture.detectChanges();
  });

  it('1. should create', () => {
    expect(component).toBeTruthy();
  });

  it('2. should check that course item was passed from the parent component', () => {
    expect(component.course).toBeDefined();
    expect(typeof component.course.id).toBe('number');
    expect(typeof component.course.title).toBe('string');
  });

  it ('3. should check that method callEditItemFromParent was called and needed param was emited', () => {
    let componentDebugElemBtnEdit  = fixture.debugElement.query(By.css('.course-item-edit'));

    component.callEditItemFromParent();
    componentDebugElemBtnEdit.triggerEventHandler('click', null);
    expect(component.course.id).toBe(1);
  });

  it ('4. should check that method deleteCourseItem was called', () => {
    let componentDebugElemBtnDel  = fixture.debugElement.query(By.css('.course-item-delete'));
    let componentNativeElemBtnDel = componentDebugElemBtnDel.nativeElement;

    spyOn(component, 'deleteCourseItem');
    componentNativeElemBtnDel.click();
    expect(component.deleteCourseItem).toHaveBeenCalled();
  })
});
