import { Component, Input, Output, OnInit, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { CourseItem } from '../../../core/course-item.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-course-item',
  templateUrl: './course-item.component.html',
  styleUrls: ['./course-item.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CourseItemComponent implements OnInit {

  @Input()
  public course: CourseItem;

  @Output() updated: EventEmitter<void> = new EventEmitter();
  @Output() removed: EventEmitter<void> = new EventEmitter();

  constructor(
    private router: Router
  ) { }

  ngOnInit() { }

  public updateCourse(): void {
    // emit course id to parent
    this.updated.emit();

    this.router.navigate(['/edit-course/', this.course.id]);
  }

  public removeCourse(): void {
    const areYouSure: boolean = confirm('Do you really want to delete this course ?');

    if (areYouSure) {
      this.removed.emit();
    }
  }
}
