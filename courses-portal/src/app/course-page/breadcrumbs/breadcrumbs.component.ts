import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.less']
})
export class BreadcrumbsComponent implements OnInit {

  constructor() { }

  public breadcrumbsHeader: string = 'Courses';

  ngOnInit() {

  }

  public setHeader(id?: number): void {
    if (id && id >= 0) {
      this.breadcrumbsHeader =  this.breadcrumbsHeader + '/Edit video course - ' + id;
    }
    else {
      this.breadcrumbsHeader = this.breadcrumbsHeader + '/New video course';
    }
  }
}
