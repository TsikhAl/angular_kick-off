import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { CoursesListItems } from '../core/courses-list-items.service';
import { CourseItem } from '../core/course-item.interface';
import { ThrowStmt } from '@angular/compiler';
import { TouchSequence } from 'selenium-webdriver';


@Component({
  selector: 'app-course-page',
  templateUrl: './course-page.component.html',
  styleUrls: ['./course-page.component.less']
})
export class CoursePageComponent implements
  OnInit,
  OnChanges {
  public availableCourses: CourseItem[] = [];
  private from: number = 0;
  private count: number = 5;
  private textFragment: string = '';
  private areCoursesAvailable: boolean = true;

  constructor(
    private coursesListService: CoursesListItems,
  ) { }

  ngOnChanges(changes: SimpleChanges) { }

  ngOnInit() {
    this.getAvailableCourses();
  }

  public getAvailableCourses(text?: string): void {
    if (this.textFragment === text && !this.areCoursesAvailable) {
      return;
    }
    else if (!text) {
      this.from = 0;
      this.areCoursesAvailable = true;
      this.textFragment = '';
    }
    else if (this.textFragment !== text && text) {
      this.textFragment = text;
    }

    this.coursesListService.getAllCourses(this.from, this.count, this.textFragment)
    .subscribe(
      (courses) => this.availableCourses = courses
    );
  }

  public updateCourse(course: CourseItem) {
    console.log(course.id);
  }

  public removeCourse(course: CourseItem) {
    this.coursesListService.removeCourse(course.id)
      .subscribe(
        () => console.log('Course has been deleted!'),
        (err) => console.log(err)
      );

    this.availableCourses = this.availableCourses.filter( item => item.id !== course.id );
  }

  public loadMore() {
    this.from += 5;
    this.coursesListService.getAllCourses(this.from, this.count, this.textFragment)
      .subscribe((newCourses) => {
        if (!newCourses.length && this.areCoursesAvailable) {
          this.from = 0;
          this.areCoursesAvailable = false;
          return;
        }
        else if (newCourses.length && this.areCoursesAvailable) {
          this.availableCourses = [
            ...this.availableCourses,
            ...newCourses
          ];
        }
      });
  }
}
