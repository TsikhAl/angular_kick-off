import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-load-more',
  templateUrl: './load-more.component.html',
  styleUrls: ['./load-more.component.less']
})
export class LoadMoreComponent implements OnInit {

  constructor() { }

  @Output() loadedMore: EventEmitter<void> = new EventEmitter();

  ngOnInit() {
  }

  public loadMore(): void {
    this.loadedMore.emit();
  }
}
