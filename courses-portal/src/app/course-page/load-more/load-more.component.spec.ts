import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { LoadMoreComponent } from './load-more.component';

describe('LoadMoreComponent', () => {
  let component: LoadMoreComponent;
  let fixture: ComponentFixture<LoadMoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadMoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadMoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.loadMore = function () {};
  });

  it('1. should create', () => {
    expect(component).toBeTruthy();
  });

  it('2. should check that load more method was called', () => {
    let componentDebugElemBtnLoadMore  = fixture.debugElement.query(By.css('.load-more-btn > a'));
    let componentNativeElemBtnLoadMore = componentDebugElemBtnLoadMore.nativeElement;

    spyOn(component, 'loadMore');
    componentNativeElemBtnLoadMore.click();
    expect(component.loadMore).toHaveBeenCalled();
  });
});
